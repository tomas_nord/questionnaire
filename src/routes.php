<?php

Route::group(['middleware' => ['web']], function () {
    Route::post('/process_questionnaire', 'TomasNord\Questionnaire\Controllers\QuestionnaireController@store');
});