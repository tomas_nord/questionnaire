<?php

namespace TomasNord\Questionnaire\Models;

use App\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

/**
 * Class Questionnaire
 *
 * @package TomasNord\Questionnaire\Models
 */
class Questionnaire extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['id', 'name'];

    /**
     * Object field to save anonymous email
     *
     * @var string
     */
    public $anonymousEmail = '';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function client(){
        return $this->belongsTo(Client::class, 'owner_id');
    }

    /**
     * Adding question to questionnaire
     *
     * @param $questionType
     * @param $questionTitle
     * @param $question
     *
     * @return $this
     */
    public function addQuestion($questionType, $questionTitle, $question)
    {
        $this->questions()
             ->create(['questionnaire_id' => $this->id, 'question_type' => $questionType, 'question_title' => $questionTitle, 'question' => $question]);

        return $this;
    }

    /**
     * Adding answer to questionnaire
     *
     * @param $questionId
     * @param $respondentId
     * @param $respondentType
     * @param $answer
     */
    public function addAnswer($questionId, $respondentId, $respondentType, $answer)
    {
        $this->answers()
             ->create(['question_id' => $questionId, 'respondent_id' => $respondentId, 'respondent_type' => $respondentType, 'answer' => $answer]);
    }

    /**
     * Returning view instance
     *
     * @return Illuminate\Support\Facades\View
     */
    public function getQuestionnaireView()
    {
        return View::make('questionnaire::questionnaire')
                   ->with(['questions' => $this->questions, 'questionnaire' => $this]);
    }

    /**
     * Get respondent.
     *
     * @param Request $request
     *
     * @return TomasNord\Questionnaire\Models\Anonymous
     */
    public function getRespondent(Request $request)
    {
        $respondent = $request->user();


        if(!$respondent) {

            if($request->has('anonymous_email')) {
                $this->anonymousEmail = $request->get('anonymous_email');
            }

            $respondent = Anonymous::create(['email' => $this->anonymousEmail]);
        }

        return $respondent;
    }

    /**
     * Add respondent
     *
     * @param $email
     *
     * @return $this
     */
    public function addAnonymousRespondentEmail($email)
    {
        $this->anonymousEmail = $email;

        return $this;
    }

}
