<?php

namespace TomasNord\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 *
 * @package TomasNord\Questionnaire\Models
 */
class Answer extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['question_id', 'respondent_id', 'respondent_type', 'answer'];

    /**
     * Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question(){
        return $this->belongsTo(Question::class);
    }

    /**
     * Receives all answers related to questionnaire id and respondent id
     * @param $questionnaireId
     * @param $respondentId
     *
     * @return \Illuminate\Support\Collection
     */
    public static function findAnswers($questionnaireId, $respondentId)
    {
        return self::where(['questionnaire_id' => $questionnaireId, 'respondent_id' => $respondentId])->get();
    }
}
