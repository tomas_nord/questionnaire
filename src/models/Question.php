<?php

namespace TomasNord\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

/**
 * Class Question
 *
 * @package TomasNord\Questionnaire\Models
 */
class Question extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['questionnaire_id', 'question_type', 'question_title', 'question'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionnaire(){
        return $this->belongsTo(Questionnaire::class);
    }

    public function answer(){
        return $this->hasMany(Answer::class);
    }

    /**
     * Getting view instance
     *
     * @return Illuminate\Support\Facades\View view
     */
    public function getView()
    {

        $questionType = new $this->question_type();

        return View::make($questionType->getQuestionView())
                   ->with(['question_id' => $this->id, 'question_type' => $this->question_type, 'question_title' => $this->question_title, 'question' => $this->question]);
    }
}
