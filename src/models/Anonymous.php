<?php

namespace TomasNord\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Anonymous
 *
 * @package TomasNord\Questionnaire\Models
 */
class Anonymous extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['email'];
}
