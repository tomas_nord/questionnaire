<?php namespace TomasNord\Questionnaire\Questions;

/**
 * Interface QuestionType
 *
 * @package TomasNord\Contracts
 */
interface QuestionTypeContract
{

    /**
     * Gets question view.
     *
     * @return mixed
     */
    public function getQuestionView();
} 