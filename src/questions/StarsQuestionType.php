<?php


namespace TomasNord\Questionnaire\Questions;


class StarsQuestionType implements QuestionTypeContract
{

    /**
     * Gets question view.
     *
     * @return mixed
     */
    public function getQuestionView()
    {
        return 'questionnaire::questions.stars';
    }
}