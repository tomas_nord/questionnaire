<?php


namespace TomasNord\Questionnaire\Questions;


class OpenQuestionType implements QuestionTypeContract
{

    /**
     * Gets question view.
     *
     * @return mixed
     */
    public function getQuestionView()
    {
        return 'questionnaire::questions.open';
    }
}