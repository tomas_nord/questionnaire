<link rel="stylesheet" type="text/css" href="vendor/questionnaire/css/questions.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/questionnaire/css/themes/fontawesome-stars.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="vendor/questionnaire/js/jquery.barrating.min.js"></script>
<script src="vendor/questionnaire/js/main.js"></script>

<form action="/process_questionnaire" method="POST" >
    {{ csrf_field() }}

    <input type="hidden" name="QuestionnaireId" value="{{ $questionnaire->id }}">

    @if($questionnaire->anonymousEmail != '')
        <input type="hidden" name="anonymous_email" value="{{ $questionnaire->anonymousEmail }}">
    @endif

    @foreach($questions as $question)
        {!! $question->getView() !!}
    @endforeach

    <input type="submit" value="submit" class="questionnaire-submit">
</form>