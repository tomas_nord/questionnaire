
<!-- Example from http://antenna.io/demo/jquery-bar-rating/examples/ -->

<div class="stars">
    <fieldset class="rating">
        <legend>{{ $question }}</legend>
        <div class="br-wrapper br-theme-fontawesome-stars">
          <select class="fontawesome-stars" name="question-{{ $question_id }}">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
    </fieldset>
</div>