<div class="open-question">
    <fieldset class="open-fieldset">
        <legend>{{ $question }}</legend>
        <div class="open-textarea">

          <textarea rows="5" cols="50" name="question-{{ $question_id }}"></textarea>
        </div>
    </fieldset>
</div>