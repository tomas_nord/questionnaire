<?php

namespace TomasNord\Questionnaire\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use TomasNord\Questionnaire\Models\Questionnaire;

/**
 * Class QuestionnaireController
 *
 * @package TomasNord\Questionnaire\Controllers
 */
class QuestionnaireController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $questionnaire = Questionnaire::find($request->get('QuestionnaireId'));

        if(!$questionnaire) {
            return false;
        }

        $respondent = $questionnaire->getRespondent($request);

        foreach($request->all() as $question => $answer) {
            if($this->isQuestion($question)) {
                $questionnaire->addAnswer($this->getQuestionId($question),
                                          $respondent->id,
                                          get_class($respondent),
                                          $answer);
            }
        }

        //Adding session flash and returning back
        Session::flash('questionnaire_message', 'success');
        Session::flash('questionnaire_id', $questionnaire->id);
        Session::flash('questionnaire_respondent', $respondent->id);

        return back();
    }

    /**
     * Checking if request element is question. Question is that element which has prefix "question"
     *
     * @param $question
     *
     * @return bool
     */
    private function isQuestion($question)
    {
        return strpos($question, 'question') !== false;
    }

    /**
     * Getting question id from example "question-23" that was received from form.
     *
     * @param $question
     *
     * @return string
     */
    private function getQuestionId($question)
    {
        return substr($question, 9);
    }
}
