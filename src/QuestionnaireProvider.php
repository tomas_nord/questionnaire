<?php

namespace TomasNord\Questionnaire;

use Illuminate\Support\ServiceProvider;
use TomasNord\Questionnaire\Models\Questionnaire;

class QuestionnaireProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';
        //Views
        $this->loadViewsFrom(__DIR__ . '/views', 'questionnaire');

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([__DIR__ . '/views' => resource_path('views/vendor/questionnaire'),]);

        $this->publishes([__DIR__ . '/views/assets' => public_path('vendor/questionnaire'),], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {

    }
}
