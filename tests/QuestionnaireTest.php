<?php

namespace TomasNord\Questionnaire\Tests;

use Faker\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use TomasNord\Questionnaire\Models\Anonymous;
use TomasNord\Questionnaire\Models\Answer;
use TomasNord\Questionnaire\Models\Questionnaire;
use TomasNord\Questionnaire\Questions\StarsQuestionType;


class QuestionnaireTest extends TestCase
{

    use DatabaseTransactions;
    private $questionnaire;

    protected function setUp()
    {

        parent::setUp();

        $this->questionnaire = Questionnaire::create(['name' => 'My new questionnaire', 'owner_id' => 1, 'owner_type' => User::class]);

        $this->questionnaire->save();
    }

    /** @test */
    public function it_adds_question_to_questionnaire()
    {
        $this->questionnaire->addQuestion(StarsQuestionType::class, "Dog", "Do you have a dog?");

        $this->assertCount(1, $this->questionnaire->questions);
    }

    /** @test */
    public function it_creates_answer_for_questionnaire()
    {
        $questionId = 1;
        $respondentId = 1;
        $respondentType = User::class;
        $answer = 'Nothing special, just answer.';

        $this->questionnaire->addAnswer($questionId, $respondentId, $respondentType, $answer);

        $this->assertCount(1, $this->questionnaire->answers->where('questionnaire_id', $this->questionnaire->id));
    }

    /** @test */
    public function it_finds_all_answers_for_questionnaire_and_respondet(){
        $questionId = 1;
        $respondentId = 1;
        $respondentType = User::class;
        $answer = 'Nothing special, just answer.';

        $this->questionnaire->addAnswer($questionId, $respondentId, $respondentType, $answer);

        $answer = 'Nothing special, just another answer.';
        $this->questionnaire->addAnswer($questionId, $respondentId, $respondentType, $answer);

        $this->assertCount(2, Answer::findAnswers($this->questionnaire->id, $respondentId));
        $this->assertEquals('Nothing special, just answer.', Answer::findAnswers($this->questionnaire->id, $respondentId)->first()->answer);
    }

    /** @test */
    public function it_returns_questionnaire_view()
    {
        $view = $this->questionnaire->getQuestionnaireView();

        $this->assertEquals('questionnaire::questionnaire', $view->name());
    }

    /** @test */
    public function it_returns_questionnaire_view_that_has_questions()
    {
        $this->questionnaire->addQuestion(StarsQuestionType::class,
                                          'Ar you stupid?',
                                          'Please describe how stupid you are?');

        $view = $this->questionnaire->getQuestionnaireView();
        $data = $view->getData();

        $this->assertInstanceOf(Collection::class, $data['questions']);
        $this->assertCount(1, $data['questions']);
    }

    /** @test */
    public function it_returns_questionnaire_view_with_questionnaire_instance()
    {
        $view = $this->questionnaire->getQuestionnaireView();
        $data = $view->getData();


        $this->assertInstanceOf(Questionnaire::class, $data['questionnaire']);
        $this->assertEquals('My new questionnaire', $data['questionnaire']->name);
    }

    /** @test */
    public function it_gets_respondet_anonymous_if_not_logged_in()
    {
        $this->assertInstanceOf(Anonymous::class, $this->questionnaire->getRespondent(new Request()));
    }

    /** @test */
    public function it_gets_respondent_email_if_it_is_set()
    {
        $this->questionnaire->addAnonymousRespondentEmail('johndie@gmail.com');

        $this->assertEquals('johndie@gmail.com', $this->questionnaire->getRespondent(new Request())->email);
    }
}
 