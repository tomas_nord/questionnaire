<?php
namespace TomasNord\Questionnaire\Tests;

use Faker\Factory;
use Orchestra\Testbench\TestCase as OrchestraTestCase;


class TestCase extends OrchestraTestCase
{

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench',
                            ['driver' => 'mysql', 'host' => 'localhost', 'database' => 'football_data', 'username' => 'root', 'password' => '', 'charset' => 'utf8', 'collation' => 'utf8_unicode_ci', 'prefix' => '', 'strict' => false,]);
    }

    protected function getPackageProviders($app)
    {
        return ['TomasNord\Questionnaire\QuestionnaireProvider'];
    }

    protected function setUp()
    {
        parent::setUp();

        $this->withFactories(__DIR__ . '/database/factories');
    }

}