<?php

namespace TomasNord\Questionnaire\Tests;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TomasNord\Questionnaire\Models\Question;
use TomasNord\Questionnaire\Questions\StarsQuestionType;


class QuestionTest extends TestCase
{

    use DatabaseTransactions;
    private $question;

    protected function setUp()
    {
        parent::setUp();

        $this->question = Question::create(['questionnaire_id' => 1, 'question_type' => StarsQuestionType::class, 'question_title' => 'What is your name?', 'question' => 'How do you do?']);

        $this->question->save();
    }

    /** @test */
    public function it_returns_view_for_question()
    {
        $view = $this->question->getView();

        $this->assertInstanceOf('Illuminate\View\View', $view);

    }

    /** @test */
    public function it_returns_stars_type_view()
    {
        $view = $this->question->getView();

        $this->assertEquals('questionnaire::questions.stars', $view->name());
    }

    /** @test */
    public function it_returns_stars_type_question()
    {
        $view = $this->question->getView();

        $data = $view->getData();

        $this->assertEquals($data['question_type'], StarsQuestionType::class);
    }

}
